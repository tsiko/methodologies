/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab2;

/**
 *
 * @author amenychtas
 */
public class Car {

    //Should the members below be public or private?
    public String make;
    //assume that the plates is a four digit number
    public int carPlates;
    //we could use the Class java.awt.Color
    public String color;

    //consider adding methods to set and get the above variables 
    //so as to ensure valid values
    
    
    public void print(){
        System.out.println("This " + color + " " + make 
                + " has " + carPlates + " license number");
    }

}
